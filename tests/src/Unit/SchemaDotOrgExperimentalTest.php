<?php

declare(strict_types=1);

namespace Drupal\Tests\schemadotorg_experimental\Unit;

use Drupal\Tests\UnitTestCase;

/**
 * Test to trigger Drupal GitLab CI pipeline.
 *
 * @group schemadotorg
 */
class SchemaDotOrgExperimentalTest extends UnitTestCase {

  /**
   * Test to trigger Drupal GitLab CI pipeline.
   */
  public function test(): void {
    $this->assertTrue(TRUE);
  }

}
