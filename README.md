Table of contents
-----------------

* [Introduction](#introduction)
* [Sub-modules](#sub-modules)
* [Installation](#installation)


Introduction
------------

Experimental integrations for the [Schema.org Blueprints module](https://www.drupal.org/project/schemadotorg)
that are not ready for prime-time.


Sub-modules
-----------

Below are the experimental sub-modules which enable different 
functionality and feature sets.

- **[Schema.org Blueprints Block Content](https://git.drupalcode.org/project/schemadotorg_experimental/-/tree/1.0.x/modules/schemadotorg_block_content_status)**  
  Displays Schema.org content blocks as status message.

- **[Schema.org Blueprints Components](https://git.drupalcode.org/project/schemadotorg_experimental/-/tree/1.0.x/modules/schemadotorg_components)**  
  Provides support for Single Directory Components (SDC) based on Schema.org types.

- **[Schema.org Blueprints Devel](https://git.drupalcode.org/project/schemadotorg_experimental/-/tree/1.0.x/modules/schemadotorg_devel)**  
  Provides development tools for the Schema.org Blueprints module.

- **[Schema.org Blueprints Editorial](https://git.drupalcode.org/project/schemadotorg_experimental/-/tree/1.0.x/modules/schemadotorg_editorial)**  
  Adds an 'Editorial information' paragraph to the sidebar on node edit forms.

- **[Schema.org Blueprints Embedded Content](https://git.drupalcode.org/project/schemadotorg_experimental/-/tree/1.0.x/modules/schemadotorg_embedded_content)**  
  Provides support for inserting rich and styled Schema.org type into HTML via CKEditor.

- **[Schema.org Blueprints Field Parts](https://git.drupalcode.org/project/schemadotorg_field_parts/-/tree/1.0.x/modules/schemadotorg_components)**  
  Allows Schema.org title/name properties to include a prefix and suffix.

- **[Schema.org Blueprints Identifier](https://git.drupalcode.org/project/schemadotorg_experimental/-/tree/1.0.x/modules/schemadotorg_identifier)**  
  Manages identifiers (https://schema.org/identifier) for Schema.org types.

- **[Schema.org Blueprints Sidebar](https://git.drupalcode.org/project/schemadotorg_experimental/-/tree/1.0.x/modules/schemadotorg_sidebar)**  
  Adds paragraph types to the sidebar on node edit forms.

- **[Schema.org Blueprints Layout Paragraphs](https://git.drupalcode.org/project/schemadotorg_experimental/-/tree/1.0.x/modules/schemadotorg_layout_paragraphs)**  
  Provides integration with the Layout Paragraphs module.


Installation
------------

Use the included [composer.libraries.json](https://git.drupalcode.org/project/schemadotorg_experimental/-/blob/1.0.x/composer.libraries.json) 
file to quickly install all sub-module dependencies.

As your Schema.org Blueprints project evolves, you may want to copy and adjust
the dependencies from the composer.libraries.json file into your project's
root composer.json.

[Watch how to install and set up the Schema.org Blueprints module](https://www.youtube.com/watch?v=Dludw8Eomh4)
