Table of contents
-----------------

* Introduction
* Features

Introduction
------------

The **Schema.org Blueprints Components** provides support for 
Single Directory Components (SDC) based on Schema.org types.


Features
--------

- https://schema.org/Action
- https://schema.org/Quotation

