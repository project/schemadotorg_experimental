Table of contents
-----------------

* Introduction
* Features
* Configuration


Introduction
------------

The **Schema.org Blueprints Block module** displays 
Schema.org content blocks as status messages.


Features
--------

- Displays content blocks as status messages.


Configuration
-------------

- Go to the Schema.org types configuration page.  
  (/admin/config/schemadotorg/settings/types#edit-schemadotorg-block-content-status)
- Go to the 'Block content status settings' details.
- Enter the message types to wrap content blocks when displayed on the page.
