Table of contents
-----------------

* Introduction
* Features

Introduction
------------

The **Schema.org Blueprints Embedded Content** provides support for 
inserting rich and styled Schema.org type into HTML via CKEditor.'


Features
--------

- Create embedded content from (SDC) components.
- Builds embedded content JSON-LD


Requirements
------------

**[Embedded Content](https://www.drupal.org/project/embedded_content)**  
Allows content editors to insert rich and styled pieces of content into CKEditor.

